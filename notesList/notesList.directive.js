
(function () {
    'use strict';

    angular
        .module('app.notesList', [])
        .directive('notesList', notesListDirective);

    function notesListDirective () {
        var directive = {
            restrict: 'E',
            templateUrl: 'notesList/notesList.directive.html',
            controller: notesListCtrl,
            controllerAs: 'vm',
            scope: true,
            bindToController: true,
            link: link
        };

        return directive;

        function link (scope, elm, attrs) {
        }
    }

    //=-=-=-=-=-=-=-
    // Controller For note list
    //=-=-=-=-=-=-=-

    notesListCtrl.$inject = ['$scope', '$document', 'notesService'];

    function notesListCtrl ($scope, $document, notesService) {
        var vm = this;
        vm.selectNote = selectNote;
        vm.removeNote = removeNote;

        // Initialize notes list.
        vm.notes = notesService.getNotes();

        // If no notes create empty note
        if (!vm.notes.length) {
            createNote();
            selectNote(0, vm.notes[0]);
        }

        // Select the first note by default on load.
        selectNote(0, vm.notes[0]);

        //=-=-=-=-=-=-=-

        // Handler event from app controller when a new
        // note is created.
        $scope.$on('new-note', function (event) {
            createNote();
            selectNote(0, vm.notes[0]);
        });

        //=-=-=-=-=-=-=-

        // Creates an empty note and puts it on the top
        // of the notes list.
        function createNote () {
            var date = Date.now();
            vm.notes.unshift({
                key: date,
                content: '',
                title: 'New Note',
                modified: date
            });
        }

        // Selects item in list and notifies the app
        // controller to load the selected note.
        function selectNote (index, note) {
            vm.selected = index;
            $scope.$emit('note-selected', note);
        }

        // Removes thte note from localStorage and
        // the notes list.
        function removeNote (index) {
            if (confirm('Are you sure you want to delete this note?')) {
                var note = vm.notes[index];
                notesService.deleteNote(note)
                vm.notes.splice(index, 1);
                if (!vm.notes.length) {
                    createNote();
                }
            }
        }
    }

})();
