# README #

## Simple notepad web application that stores notes in local storage. ##

Written in Angular JS, the app provides the following features:

* Responsive layout.
* Full screen view.
* Search for content in notes.
* Auto-save after typing.

### To Run ###

execute in the apps directory: python -m SimpleHTTPServer 8000