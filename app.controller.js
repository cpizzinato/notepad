
(function () {
    'use strict';

    angular
        .module('notepadApp')
        .controller('appCtrl', appCtrl);

    appCtrl.$inject = ['$scope', '$window', '$document', '$timeout', 'notesService'];

    function appCtrl ($scope, $window, $document, $timeout, notesService) {
        var vm = this;
        var textarea = $document[0].getElementById('note');
        var sidebar = $document[0].getElementsByClassName('sidebar');

        var MIN_DESKTOP_WIDTH = 1025;

        vm.createNote = createNote;
        vm.toggleSideBar = toggleSideBar;

        init();

        //=-=-=-=-=-=-
        // Event Handlers
        //=-=-=-=-=-=-

        $scope.$on('note-selected', function (event, note) {
            vm.selectedNote = note;
            textarea.focus();
        });

        angular.element($window).on('resize', function(event) {
            if (event.target.innerWidth < MIN_DESKTOP_WIDTH) {
                $scope.$apply(function () {
                    vm.sideBarOpen = false;
                });
            }
        });

        $document.on('click', function (event) {
            if (vm.sideBarOpen && event.target === textarea) {
                $scope.$apply(function () {
                    toggleSideBar();
                });
            }
        });

        //=-=-=-=-=-=-

        // Throttles the save so that it isn't saving on
        // every key press. Timer continuously gets cancelled
        // until the user stops typing for <timeout> or default
        // 300ms.
        function throttledSave(cb, timeout) {
          var timer = null;
          timeout = timeout || 300;
          return function () {
            var context = this, args = arguments;
            //dynamically udates the note title to only the first line.
            updateNoteTitle();
            $timeout.cancel(timer);
            timer = $timeout(function () {
              cb.apply(context, args);
          }, timeout);
          };
        }

        //Updates the notes title to match the first line in the note.
        function updateNoteTitle() {
            vm.selectedNote.title = textarea.value.split('\n')[0] || 'New Note';
        }

        // Initialize autosave and sidebar status.
        function init() {
            textarea.addEventListener('keyup', throttledSave(function() {
                notesService.saveNote(vm.selectedNote)
            }, 500));

            vm.sideBarOpen = $window.innerWidth > MIN_DESKTOP_WIDTH;
            vm.localStorageError = !Modernizr.localstorage;
        }

        // notifies notes list to add a new note.
        function createNote () {
            $scope.$broadcast('new-note');
            textarea.focus();
        }

        // Opens and closes the sidebar.
        function toggleSideBar () {
            vm.sideBarOpen = !vm.sideBarOpen;
        }
    }
})();
