
(function () {
    'use strict';

    angular
        .module('app.notes.service', [])
        .factory('notesService', notesService);

    notesService.$inject = ['localStorageService'];

    function notesService (localStorageService) {

        var service = {
            getNotes: getNotes,
            saveNote: saveNote,
            deleteNote: deleteNote
        };

        return service;

        //=-=-=-=-=-=-=-

        // Retrieves all notes from localStorage.
        function getNotes () {
            var keys = localStorageService.keys();

            if (keys.length === 0) {
                return [];
            }

            var notes = Array.prototype.map.call(keys, function (key) {
                var note = JSON.parse(localStorageService.get(key));
                note.key = key;
                return note;
            });

            return notes;
        }

        // Saves the note to localStorage.
        function saveNote (note) {
            //Update modified time to now.
            note.modified = Date.now();
            localStorageService.set(note.key, JSON.stringify(note));
        }

        // Remove the note from localStorage.
        function deleteNote (note) {
            localStorageService.remove(note.key);
        }
    }
})();
