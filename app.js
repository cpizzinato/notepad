
(function () {
    'use strict';

    angular
        .module('notepadApp', [
            'ngAnimate',
            'app.notesList',
            'app.notes.service',
            'LocalStorageModule'
        ])
        .config(['localStorageServiceProvider', function(localStorageServiceProvider){
          localStorageServiceProvider.setPrefix('notes');
        }]);
})();
